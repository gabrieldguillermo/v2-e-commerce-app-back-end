const express = require("express");
const router = express.Router();
const auth = require("../auth");

const {
  registerUser,
  checkEmail,
  loginUser,
  userDetails,
  changePassword,
  editProfile,
  allUsers,
  getUser,
  userStatus,
} = require("../controllers/userController");

// user registration
router.post("/register", registerUser);

//check user email
router.post("/checkEmail", checkEmail);

// user login
router.post("/login", loginUser);

//get user details  (users and admin)
router.get("/details", auth.verify, userDetails);
router.get("/all", auth.verify, allUsers);

router.patch("/changePassword", auth.verify, changePassword);
//get specific user
router.get("/:userId", auth.verify, getUser);
//edit profile
router.put("/details/:userId", auth.verify, editProfile);
//update user status (admin)
router.patch("/details/:userId", auth.verify, userStatus);

module.exports = router;
