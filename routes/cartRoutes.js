const express = require("express");
const router = express.Router();
const auth = require("../auth");

const { addToCart, getCart, getCartById, updateCart, deleteCart } = require("../controllers/cartController");

router.post("/", auth.verify, addToCart);
router.get("/:userId", auth.verify, getCart);
router.get("/:useId/:cartId", auth.verify, getCartById);
router.put("/update/:cartId", auth.verify, updateCart);
router.delete("/delete/:cartId", auth.verify, deleteCart);

module.exports = router;
