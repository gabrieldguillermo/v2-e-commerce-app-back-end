const mongoose = require('mongoose');

const productSchema = new mongoose.Schema({
    productName: {
        type: String,
        required: [true, "Product Name is required"]
    },
    description: {
        type: String,
        required: [true, "Description is required"],
    },
    imageUrl:{
        type:String,
        required: [true ,"image url required"]
    },
    // category: {
    //     type: [{type:String}],
    //     validate: {
    //         validator: arr => arr.length > 0,
    //         message: () => "Category field is required"
    //     }
    // },
    price: {
        type: Number,
        required: [true, "Price is required"]
    },
    quantity: {
        type: Number,
        required: [true, "Count is required"]
    },
    isActive: {
        type: Boolean,
        default: true
    },
    createdOn: {
        type: Date,
        default: Date.now
    }

});

module.exports = mongoose.model("Product", productSchema);
