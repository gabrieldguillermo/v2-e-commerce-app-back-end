const mongoose = require("mongoose");

const cartSchema = new mongoose.Schema({
  userId: {
    type: String,
    required: [true, "user ID is required"],
  },
  productId: {
    type: String,
    required: [true, "product ID is required"],
  },
  productName: {
    type: String,
    required: [true, "product name is required"],
  },
  quantity: {
    type: Number,
    default: 1,
  },
  price: {
    type: Number,
    default: 0,
  },
  imageUrl: {
    type: String,
    required: [true, "image url required"],
  },
  isOrdered: {
    type: Boolean,
    default: false,
  },
  date: {
    type: Date,
    default: Date.now,
  },
});

module.exports = mongoose.model("Cart", cartSchema);
