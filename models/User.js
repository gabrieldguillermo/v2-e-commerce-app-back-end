const mongoose = require("mongoose");

const userSchema = new mongoose.Schema({
  firstName: {
    type: String,
    required: [true, "E-mail is required"],
  },
  lastName: {
    type: String,
    required: [true, "E-mail is required"],
  },
  email: {
    type: String,
    required: [true, "E-mail is required"],
    unique: true,
  },
  password: {
    type: String,
    required: [true, "password is required"],
  },
  isAdmin: {
    type: Boolean,
    default: false,
  },
  mobileNo: {
    type: String,
    default: null,
  },
  address: {
    type: String,
    default: null,
  },
});

module.exports = mongoose.model("User", userSchema);
