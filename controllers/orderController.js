const Product = require("../models/Product");
const Order = require("../models/Order");
const Cart = require("../models/Cart");
const auth = require("../auth");

const orderProduct = async (req, res) => {
  try {
    const verifiedUser = auth.decode(req.headers.authorization);
    const products = req.body.products;
    if (verifiedUser.isAdmin) {
      return res.send("You are an Admin");
    }

    //order schema
    const newOrder = new Order({
      userId: verifiedUser.id,
      products: products,
      totalAmount: req.body.totalAmount,
    });
    await newOrder.save();

    for (let i = 0; i < products.length; i++) {
      let id = products[i].productId;
      let quantity = products[i].quantity;
      await Product.findByIdAndUpdate(id, { $inc: { quantity: -quantity } });
    }

    return res.send(true);
  } catch (error) {
    return res.send(false);
  }
};

//ordered products
const orderedProduct = async (req, res) => {
  try {
    const verifiedUser = auth.decode(req.headers.authorization);

    if (verifiedUser.isAdmin) {
      const getUsersAllOrder = await Order.find({}, { __v: 0 });
      return res.send(getUsersAllOrder);
    }

    const getAllOrder = await Order.find({ userId: verifiedUser.id }).sort({ purchasedOn: -1 });
    return res.send(getAllOrder);
  } catch (error) {
    return res.send(error.message);
  }
};

const userOrders = async (req, res) => {
  try {
    const verifiedUser = auth.decode(req.headers.authorization);
    if (!verifiedUser.isAdmin) {
      return res.send(false);
    }
    const getUserOrders = await Order.find({ userId: req.params.userId }).sort({ purchasedOn: -1 });
    return res.send(getUserOrders);
  } catch (error) {
    return res.send(false);
  }
};

module.exports = {
  orderProduct,
  userOrders,
  orderedProduct,
};
