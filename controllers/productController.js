// const User = require('../models/User');
const Product = require("../models/Product");
// const bcrypt = require('bcrypt');
const auth = require("../auth");

// add new product for admin
const addProduct = async (req, res) => {
  try {
    const verifiedUser = auth.decode(req.headers.authorization);
    const findDuplicate = await Product.findOne({ productName: req.body.name });

    if (!verifiedUser.isAdmin) {
      return res.send({ notAdmin: "You are not an Admin" });
    }

    if (findDuplicate !== null) {
      return res.send({ duplicate: "Product Name is already been used" });
    }

    let newProduct = new Product({
      productName: req.body.name,
      description: req.body.description,
      price: req.body.price,
      imageUrl: req.body.imageUrl,
      quantity: req.body.quantity,
    });

    await newProduct.save();
    return res.send(true);
  } catch (error) {
    return res.send(error);
  }
};

//get all active products
const getAllProduct = async (req, res) => {
  try {
    const verifiedUser = auth.decode(req.headers.authorization);

    if (verifiedUser.isAdmin) {
      const allProducts = await Product.find().sort({ createdOn: -1 });
      return res.send(allProducts);
    }
    return res.send(false);
  } catch (error) {
    return res.send(false);
  }
};

//user view product
const viewProducts = async (req, res) => {
  try {
    const products = await Product.find({ isActive: true });
    // console.log(products)
    return res.send(products);
  } catch (error) {
    return res.send(error);
  }
};

//get specific product
const getProduct = async (req, res) => {
  try {
    const product = await Product.findById(req.params.productId);
    return res.send(product);
  } catch (error) {
    return res.send(error);
  }
};

//update product
const updateProduct = async (req, res) => {
  try {
    const verifiedUser = auth.decode(req.headers.authorization);
    if (!verifiedUser.isAdmin) {
      return res.send(false);
    }
    await Product.findByIdAndUpdate(req.params.productId, req.body);
    return res.send(true);
  } catch (error) {
    return res.send(false);
  }
};

//archive specific product
const archiveProduct = async (req, res) => {
  try {
    const verifiedUser = auth.decode(req.headers.authorization);
    if (!verifiedUser.isAdmin) {
      return res.send(false);
    }
    await Product.findByIdAndUpdate(req.params.productId, { isActive: false });
    return res.send(true);
  } catch (error) {
    return res.send(false);
  }
};
//unarchive specific product
const unArchiveProduct = async (req, res) => {
  try {
    const verifiedUser = auth.decode(req.headers.authorization);
    if (!verifiedUser.isAdmin) {
      return res.send(false);
    }
    await Product.findByIdAndUpdate(req.params.productId, { isActive: true });
    return res.send(true);
  } catch (error) {
    return res.send(false);
  }
};
module.exports = {
  addProduct,
  getAllProduct,
  getProduct,
  viewProducts,
  updateProduct,
  archiveProduct,
  unArchiveProduct,
};
