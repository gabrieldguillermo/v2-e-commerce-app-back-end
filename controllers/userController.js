const User = require("../models/User");
const bcrypt = require("bcrypt");
const auth = require("../auth");

// registration
const registerUser = async (req, res) => {
  try {
    if (Object.keys(req.body).length === 0) {
      return res.send("req body is empty");
    }
    let newUser = new User({
      firstName: req.body.firstName,
      lastName: req.body.lastName,
      email: req.body.email,
      password: bcrypt.hashSync(req.body.password, 10),
    });

    await newUser.save();
    return res.send(true);
  } catch (error) {
    return res.send(false);
  }
};

//verify user email if already exist in the data base

const checkEmail = async (req, res) => {
  try {
    const findEmail = await User.findOne({ email: req.body.email });

    if (findEmail) {
      return res.send(true);
    } else {
      return res.send(false);
    }
  } catch (error) {
    return res.send(false);
  }
};

//login user
const loginUser = async (req, res) => {
  try {
    const findUser = await User.findOne({ email: req.body.email });
    if (findUser == null) {
      return res.send(false);
    }
    const isPasswordCorrect = bcrypt.compareSync(req.body.password, findUser.password);
    if (isPasswordCorrect) {
      return res.send({ access: auth.createAccessToken(findUser) });
    } else {
      return res.send(false);
    }
  } catch (error) {
    return res.send(error);
  }
};

//get user information/details
const userDetails = async (req, res) => {
  try {
    const verifiedUser = auth.decode(req.headers.authorization);

    if (!verifiedUser) {
      return res.send(false);
    }
    const data = await User.findById(verifiedUser.id);
    return res.send(data);
  } catch (error) {
    return res.send(false);
  }
};

//get specific user
const getUser = async (req, res) => {
  try {
    const verifiedUser = auth.decode(req.headers.authorization);

    if (!verifiedUser.isAdmin) {
      return res.send(false);
    }
    const data = await User.findById(req.params.userId);
    return res.send(data);
  } catch (error) {
    console.log(error);
    return res.send(false);
  }
};
const changePassword = async (req, res) => {
  try {
    const verifiedUser = auth.decode(req.headers.authorization);
    if (!verifiedUser) {
      return res.send(false);
    }

    const findUser = await User.findOne({ _id: verifiedUser.id });
    if (findUser == null) {
      return res.send(false);
    }

    const isPasswordCorrect = bcrypt.compareSync(req.body.password, findUser.password);
    if (isPasswordCorrect) {
      const edit = await User.findByIdAndUpdate(findUser._id, { password: bcrypt.hashSync(req.body.newPassword, 10) });
      return res.send(true);
    } else {
      return res.send(false);
    }
  } catch (error) {
    return res.send(false);
  }
};

const editProfile = async (req, res) => {
  try {
    const verifiedUser = auth.decode(req.headers.authorization);

    if (!verifiedUser) {
      return res.send(false);
    }
    await User.findByIdAndUpdate(req.params.userId, req.body);
    return res.send(true);
  } catch (error) {
    return res.send(false);
  }
};

//get all users
const allUsers = async (req, res) => {
  try {
    const verifiedUser = auth.decode(req.headers.authorization);

    if (!verifiedUser.isAdmin) {
      return res.send(false);
    }

    const getUsers = await User.find({ _id: { $ne: verifiedUser.id } });
    return res.send(getUsers);
  } catch (error) {
    return res.send(false);
  }
};

//update user status
const userStatus = async (req, res) => {
  try {
    const verifiedUser = auth.decode(req.headers.authorization);

    if (!verifiedUser.isAdmin) {
      return res.send(false);
    }

    await User.findByIdAndUpdate(req.params.userId, { isAdmin: req.body.isAdmin });
    return res.send(true);
  } catch (error) {
    return res.send(error.message);
  }
};

module.exports = {
  registerUser,
  checkEmail,
  loginUser,
  userDetails,
  changePassword,
  editProfile,
  allUsers,
  getUser,
  userStatus,
};
