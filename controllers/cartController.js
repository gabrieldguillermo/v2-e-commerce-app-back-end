const Product = require("../models/Product");

const Cart = require("../models/Cart");

const auth = require("../auth");

const addToCart = async (req, res) => {
  try {
    let cart = new Cart({
      userId: req.body.userId,
      productId: req.body.productId,
      productName: req.body.productName,
      quantity: req.body.quantity,
      price: req.body.price,
      imageUrl: req.body.imageUrl,
    });
    const checkCart = await Cart.findOne({ userId: req.body.userId, productId: req.body.productId });

    if (checkCart) {
      const sum = req.body.quantity + checkCart.quantity;

      const update = await Cart.findByIdAndUpdate(checkCart._id, {
        quantity: sum,
        date: Date.now(),
      });
      if (update !== null) {
        return res.send(true);
      }
    } else {
      await cart.save();
      return res.send(true);
    }
  } catch (error) {
    return res.send(false);
  }
};
const getCart = async (req, res) => {
  try {
    const fetchCart = await Cart.find({ userId: req.params.userId }).sort({ date: -1 });
    // const product = await Product.findOne({ _id: data.productId });
    return res.send(fetchCart);
  } catch (error) {
    return res.send(false);
  }
};

const getCartById = async (req, res) => {
  try {
    const data = await Cart.findOne({ _id: req.params.cartId });
    const product = await Product.findOne({ _id: data.productId });
    return res.send({ data, productLeft: product.quantity });
  } catch (error) {
    return res.send(false);
  }
};

const updateCart = async (req, res) => {
  try {
    const update = await Cart.findByIdAndUpdate(req.params.cartId, req.body);
    return res.send(update);
  } catch (error) {
    return res.send(false);
  }
};

const deleteCart = async (req, res) => {
  try {
    const data = req.params.cartId;
    const ids = data.split(",");

    const deleteCart = await Cart.deleteMany({ _id: { $in: ids } });
    if (deleteCart) return res.send(true);
    return res.send(true);
  } catch (error) {
    return res.send(false);
  }
};

module.exports = {
  addToCart,
  getCart,
  getCartById,
  updateCart,
  deleteCart,
};
