const mongoose = require('mongoose');

const connectionString= "mongodb+srv://gabrieldguillermo:admin123@zuitt-batch-197.w7cnjly.mongodb.net/capstone3?retryWrites=true&w=majority";

 
 const connectDB = () => {
    return mongoose.connect(connectionString, {
        useNewUrlParser: true,
        useUnifiedTopology: true,
        autoIndex: true
    });
}

const dbConn = mongoose.connection

dbConn.on('error', () => console.error('Connection Error!'));
dbConn.once('open', () => console.log('Connected to MongoDb!'));


module.exports = connectDB

